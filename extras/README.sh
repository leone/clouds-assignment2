# Save this configuration file in /etc/systemd/system/flaskapp.service
sudo cp flaskapp.service /etc/systemd/system/ # Copy the service file
sudo systemctl daemon-reload # Reload the systemd manager configuration
sudo systemctl enable flaskapp # Enable the service so that it starts on boot
sudo systemctl restart flaskapp # Start the service
sudo systemctl status flaskapp # Check that the service is active
