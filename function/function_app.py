import azure.functions as func
import logging
from integrator import integrate
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

@app.route(route='numericalintegralservice/{lower}/{upper}', methods=['GET'])
def numerical_integration_service(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    lower = float(req.route_params.get("lower"))
    upper = float(req.route_params.get("upper"))
    
    table = "<table>""<tr><th>N</th><th>Integral</th></tr>"
    for exp in range(1, 7):
        N = int(math.pow(10, exp))
        integral = integrate(lambda x: abs(math.sin(x)), lower, upper, N)
        table += f"<tr><td>{N}</td><td>{integral:.8f}</td></tr>"
    table += "</table>"
    
    html_content = f"<html><body>{table}</body></html>"
    
    return func.HttpResponse(html_content, status_code=200, headers={"Content-Type": "text/html"})
