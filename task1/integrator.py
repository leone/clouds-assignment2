def integrate(f, start, end, num_steps):
    step_size = (end - start) / num_steps
    integral = 0

    x = start
    for _ in range(num_steps):
        integral += f(x)
        x += step_size

    integral *= step_size  # multiply at the end for efficiency
    return integral
