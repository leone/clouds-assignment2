import math
from flask import Flask
from integrator import integrate

app = Flask(__name__)

@app.route('/numericalintegralservice/<lower>/<upper>')
def integrator(lower, upper):
    lower = float(lower)
    upper = float(upper)

    table = "<table>""<tr><th>N</th><th>Integral</th></tr>"
    for exp in range(1, 7):
        N = int(math.pow(10, exp))
        integral = integrate(lambda x: abs(math.sin(x)), lower, upper, N)
        table += f"<tr><td>{N}</td><td>{integral:.8f}</td></tr>"
    table += "</table>"
    
    return table

if __name__ == '__main__':
    app.run()
