import pandas as pd
import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt

# Load the CSV files
local_stats = pd.read_csv('local_stats_history.csv')
scaleset_stats = pd.read_csv('scaleset_stats_history.csv')
webapp_stats = pd.read_csv('webapp_stats_history.csv')
function_stats = pd.read_csv('function_stats_history.csv')

# Downsample the webapp_stats data
webapp_stats_downsampled = webapp_stats.iloc[::5]
webapp_stats_downsampled = webapp_stats_downsampled[:-8]

# Plot the graph
timestamps = np.arange(1, 172)

plt.plot(timestamps, local_stats['Requests/s']-local_stats['Failures/s'], label='Local')
plt.plot(timestamps, scaleset_stats['Requests/s']-scaleset_stats['Failures/s'], label='Azure VM ScaleSet')
plt.plot(timestamps, webapp_stats_downsampled['Requests/s']-webapp_stats_downsampled['Failures/s'], label='Azure WebApp')
plt.plot(timestamps, function_stats['Requests/s']-function_stats['Failures/s'], label='Azure Function')

# Add labels and title
plt.xlabel('Time (seconds)')
plt.ylabel('Successfull Requests per Second')
plt.title('Successfull Requests per Second Over Time')
plt.grid(True)

# Add legend
plt.legend()

# Save the graph with a unique timestamp in the file name
plt.savefig(f'graph_{int(time.time())}.jpg')

# Show the graph
plt.show()
