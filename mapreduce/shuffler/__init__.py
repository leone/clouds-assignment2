from typing import List, Tuple

def main(input: List[Tuple[str, int]]) -> List[Tuple[str, List[int]]]:
    # group values by key and concatenate the values of each key into a list
    tmp = dict()
    for k, v in input:
        if k not in tmp:
            tmp[k] = []
        tmp[k].append(v)
    return list(tmp.items())