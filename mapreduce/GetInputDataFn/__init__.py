from typing import Tuple, List
from azure.storage.blob import BlobServiceClient
import logging

def main(input) -> List[Tuple[int, str]]:
    """
        input: Tuple[str, str] = (connection_string, container_name)
    """
    # connect to blob storage and retrieve the container
    blob_service_client = BlobServiceClient.from_connection_string(input[0])
    blob_container_client = blob_service_client.get_container_client(input[1])
    if not blob_container_client.exists():
        raise Exception(f"Blob container '{input[1]}' does not exist.")
    
    data = []
    blobs = blob_container_client.list_blobs()
    # iterate over blobs in container
    for blob in blobs:
        logging.info(f"Reading blob '{blob.name}'.")
        # download blob as text and split into lines
        blob_client = blob_container_client.get_blob_client(blob.name)
        blob_data = blob_client.download_blob()
        blob_as_text = blob_data.content_as_text().splitlines()
        # put lines into data list
        for line in blob_as_text:
            processed_line = line.rstrip('\n').lower()
            data.append(processed_line)
    # return data as list of tuples with index
    return list(enumerate(data))
