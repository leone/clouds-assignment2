from typing import List, Tuple

def main(input) -> List[Tuple[str, int]]:
    # map each word to (word, 1) tuple
    words = input[1].split()
    return [(word, 1) for word in words]
