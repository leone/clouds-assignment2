from typing import List, Tuple

def main(input) -> Tuple[str, int]:
    # reduce each key to (key, sum(values)) tuple
    total = sum(input[1])
    return (input[0], total)
